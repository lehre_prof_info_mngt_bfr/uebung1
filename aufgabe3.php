<?php
	require_once 'aufgabe2.php';
	class RatingsJSON extends Ratings {
		function addJsonRating($json){
			$params = json_decode($json);
			parent::addRating($params->url, $params->rating, $params->comment);
		}

		function getRatings($url){
			return json_encode(parent::getRatings($url));
		}

		function getAvgRating($url){
			return json_encode(parent::getAvgRating($url));
		}

	}

 	$url = 'wifa.uni-leipzig.de';
 	$rating = rand(1,5);
	$ratings = new RatingsJSON(MYSQL_HOST, MYSQL_USER, MYSQL_PW);
	$ratings->addJsonRating('{"url" : "http://google.de", "rating" : 1, "comment" : "asdf"}');
	print("<p>".$ratings->getRatings('http://google.de'));
 	print("<p>".$ratings->getAvgRating($url));

?>
