<?php
	require_once("constants.php");
	class Ratings {
		private static $insertStmtString = "INSERT INTO ratings (url, rating, comment) values (:url, :rating, :comment)";
		private static $selectStmtString = "Select url, rating, comment from ratings WHERE url = :url";
		private static $selectAvgStmtString = "Select avg(rating) as avgrating from ratings where url = :url";

		private $con, $insertStmt, $selectStmt, $selectAvgStmt;

		public function __construct($host, $user, $pw){
			$this->con = new PDO('mysql:host=' . $host . ';dbname=ratings', $user, $pw);
			$this->insertStmt = $this->con->prepare(Ratings::$insertStmtString);
			$this->selectStmt = $this->con->prepare(Ratings::$selectStmtString);
			$this->selectAvgStmt = $this->con->prepare(Ratings::$selectAvgStmtString);
		}

		function addRating($url, $rating, $comment){
			$stmt = $this->insertStmt;
			$stmt->bindParam(':url', $url);
			$stmt->bindParam(':rating', $rating);
			$stmt->bindParam(':comment', $comment);
			$stmt->execute();
		}

		function getRatings($url){
			$stmt = $this->selectStmt;
			$stmt->bindParam(':url', $url);
			$stmt->execute();
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		function getAvgRating($url){
			$stmt = $this->selectAvgStmt;
			$stmt->bindParam(':url', $url);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result['avgrating'];
		}
	}

	function test_ratings(){
		$url = 'wifa.uni-leipzig.de';
		$rating = rand(1,5);
		$ratings = new Ratings(MYSQL_HOST, MYSQL_USER, MYSQL_PW);
		$ratings->addRating($url, $rating, 'bla, bla bla bla, bla!');
		print_r($ratings->getRatings($url));
		echo "<br/>";
		print_r($ratings->getAvgRating($url));
	}
	//test_ratings();
?>
