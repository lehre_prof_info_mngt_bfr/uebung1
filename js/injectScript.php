<?php
  require_once("../constants.php");
?>
setTimeout(function() {
  proceed();
}, 1000);

/*
 * Dieses Script injeziert einen Div-Container mit der ID "ratewindow" in die aufgerufene Seite.
 * Anschließend wird der Inhalt der Datei "aufgabe4.php" in das div geladen.
 */
function proceed() {
	if(!$("#ratewindow").length){
		$("body").append("<div class='modal fade' id='ratewindow'></div>");
  }
  $("#ratewindow").load('<?php echo ABSOLUTE_URL_TO_PROJECT . "aufgabe4.php"; ?>');
  $("#ratewindow").show();

};
