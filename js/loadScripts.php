<?php
  require_once("../constants.php");
?>
/*
 * Läd JQuery in das aktuelle Dokument
 */
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'http://code.jquery.com/jquery-latest.min.js';
document.body.appendChild(script);

/*
 * Läd das Inject-Script in das aktuelle Dokument
 */
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = '<?php echo ABSOLUTE_URL_TO_PROJECT . 'js/injectScript.php'; ?>';
document.body.appendChild(script);
